# **Tangible AI Internship Projects Report**

By: Greg Thompson

 

## **Summary**

Throughout the summer internship, I focused on understanding how to how to address two concerns. First, I wanted to understand how chatbots work to help inform my conversation designs. Second, I wanted to learn how to use interaction data to assist with the analysis and improvement of conversational flows. To explore these two issues, I worked on a range of projects from conversation design and testing to Python development using a variety of tools. Over the 10 weeks, under the guidance of Maria Dyshel and Hobson Lane, I gained exposure to conversational UX guidelines, chatbot design/development tools, Git workflows, Python programming concepts, and new ways of thinking through problems and communicating. These experiences and discussions helped me see where design and development can support each other. For example, I learned that exploring different development tools can help broaden one’s perspective regarding conversation design. The broader perspective can help one choose the right tools for the right job. Additionally, in working with different platforms, I could see how the platform affected testing and data collection as well as different strategies for approaching testing. While still a novice, I have been able to run the full development of a chatbot from ideation to design to development and even to deployment of sorts. The progress I have made through this internship has laid a solid foundation for me to build upon and continue my exploration of chatbot design and development.

 

## **Syndee** 

**Description:** Syndee is a chatbot that aims to help people deal with feelings of Imposter Syndrome. This project is being developed in Landbot now. At first, I started off exploring Syndee and fixing typos to learn the system. After getting familiar with the bot and the platform, I designed and implemented a conversation flow designed to help a user reframe their perspective on negative thoughts. After this, Maria and I worked on the analytics scheme, setting up triggers to log certain events and developing a test set document. Currently, we are working through the document to test the bot’s analytics events.

 

**Deliverable:**

- [Syndee Tool Flow](https://docs.google.com/spreadsheets/d/1Ld7kHZ-jKoxUS0Ak_nS7xEZqgwUzqVscBfzYZOV5R78/edit?usp=sharing)



**Insights:**

* It is critical to understand the tool that you are using for development when you are designing because you need to understand its limitations and capabilities.

* Conversation design principles can be easy to understand, but difficult to implement. For example, Grice’s Maxims cover quantity, quality, relation, and manner. Refining the conversation flow in line with the maxims takes a lot of consideration about the relationship between the audience and the conversation moves.

* Chatbots can accomplish many different broad and narrow tasks that people can find useful beyond commerce. In truth, I had not had much exposure to chatbots. As I worked on Syndee, Maria pointed me to Woobot. Exposure to these different bots helped give me ideas about what kinds of bots I would be interested in working on.

 



## **Rasa**

**Description:** Rasa is an open-source platform for developing conversational experiences. For this project, I developed a quiz bot to help people test their knowledge of Bash commands. The project had two different focus areas: 1) to make a useful educational tool and 2) to use many components of Rasa. The bot offered quizzes at different levels – beginner, intermediate, and advanced. It kept score of correct responses and offered feedback to help students improve. Further, to connect the bot to the broader learning environment an instructor might be working within, the bot could send interaction data via the xAPI learning data specification to a special database called a Learning Record Store.

 

**Deliverable:**

* [Rasa Development: Introduction to Rasa](https://chatbotdesign.substack.com/p/rasa-development-introduction-to)
* [Rasa Quizbot: Setting Up Responses (Part 1)](https://chatbotdesign.substack.com/p/rasa-quizbot-setting-up-responses)
* [Rasa Quizbot - Adding Interaction with Forms (Part 2)](https://chatbotdesign.substack.com/p/rasa-quizbot-adding-interaction-with)
* [Rasa Quizbot – Adding Custom Actions for Form Validation (Part 3)](https://chatbotdesign.substack.com/p/rasa-quizbot-adding-custom-actions)
* [Rasa Bash Quizbot GitLab Repository](https://gitlab.com/thompsgj/rasa-quizbot)
 

**Insights:**

* When you get stuck, sometimes the reason is that you need to adjust your perspective about how the tool operates.

* Documentation and examples can be a valuable resource to understand how others approach a similar task and whether your own approach is viable.

* I gained a better practical sense of each of the chatbot’s components, how they influence the conversation, and how to develop them. For example, intents, entities, and natural language understanding were pretty abstract, but I have a better sense of what they contribute to a chatbot and how they impact design.

* A designer can help a developer by considering what variables will be used and starting to integrate those variables into the design specifications (or otherwise indicating them for developers).

 

 

## **Qary**

**Description:** Qary is an open-source chatbot framework that can be used for a variety of prosocial purposes. To better understand the way chatbots worked, I explored Qary’s code and performed a few development tasks under Hobson’s guidance. These tasks included adding a parameter, writing code to generate messages for testing, and migrating functions.

 

**Deliverable:**

* [Parameter Work Merge Request](https://gitlab.com/tangibleai/qary/-/merge_requests/81)
* [Move Functions](https://gitlab.com/tangibleai/qary/-/merge_requests/83)

 

**Insights:**

* I got a sense of how chatbots operated, such as how important model selection is for different tasks and how skills are built.

* Git is a powerful, yet complicated tool for collaboration and proactive communication is necessary to ensure development proceeds smoothly. Git testing and merging are a part of this healthy (asynchronous) communication.

* Simple Python skills are enough to make some contributions to some codebases and can help you, as a designer, have conversations about how the system works. This understanding can make you aware of the capabilities and limitations of a chatbot platform you are designing within.

* Conducting “simple” tasks like moving functions from one file to another is a great way to practice coding skills, gain some insight into how to program, and can keep you motivated to learn.

* Well-written tests help keep you accountable when writing and revising code and keep you from accidentally entering a mistake into an application, so it’s important to learn how to write them.

 

 

## **Mycroft**

**Description:** Mycroft is an open-source voice assistant. It offers strong support for the Raspberry Pi via Picroft (Mycroft + Raspberry Pi). For this project, I hoped to create a storytelling bot. Due to the other projects I worked on, I only had time to set up Mycroft on the Raspberry Pi and to create a very simple test skill.

 

**Deliverable**

* [Introduction to Mycroft: Setup](https://chatbotdesign.substack.com/p/introduction-to-mycroft-setup-and)

 

**Insights:**

* Good documentation and actual working project examples are critical to making a platform easy to work with.

* I gained some familiarity with Raspberry Pi.

 
