Welcome to the Spring 2021 Internship at Tangible AI! We have a diverse group of interns from at least 4 time zones. You'll get to know each other more as the weeks progress and you help each other along the way.
In the future you will receive more personalized messages from Tanbot on Slack. But to do that you need to first join the Tangible AI Slack and find the #interns channel:

https://join.slack.com/t/tangibleai/shared_invite/zt-oaj57xg1-RvhnKzlRQhoJCmKF6lv8iQ
Also, please join these slack channels so you can ask questions about Python or Machine Learning in the broader San Diego community:
D ML Slack: https://join.slack.com/t/sdmachinelearning/shared_invite/zt-6b0ojqdz-9bG7tyJMddVHZ3Zm9IajJA .
SD Python Slack: https://join.slack.com/t/pythonsd/shared_invite/zt-o3c31lad-JUmkE6NO7TnI6DT2aZXz2A
If you haven't already, please set up a weekly mentoring session with Hobson.

http://calendly.com/hobs/
You can also meet with Dwayne or Maria as needed. Winston and Jon Sundin have also offered to help you along the way.

# Skills Checklist Exercise
Your exercise this week is a self-assessment.This will help us craft an internship with your learning needs and help you track your learning.
TLDR; Create a fork of Tangible AI's `team` repository on GitLab, copy and fill out the `interns/skills-checklist/blank.md` file, then issue a Merge Request back to Tangible AI's repository.

## Detailed instructions

Try not to use the links shown here. That way you can learn how to find repositories and files on gitlab.com yourself.

- Create a gitlab account- Fork the [tangibleai/team](http://gitlab.com/tangibleai/team) repository
- Find the [`blank.md`](http://gitlab.com/tangibleai/team/-/blob/master/interns/skills-checklist/blank.md) file in the `interns/skills-checklist` directory
- Click the [Edit] or [Raw] button in the gitlab GUI
- Select all of the checklist text and copy it
- Create a new file in the `interns/skills-checklist` directory.
- Check off the skills that you think you have mastered
- Commit your file to your fork of the repo

### Bonus

- Create a `[Merge Request]` (MR) from your `[Fork]` to Tangible AI version of the `team/` repository.
- Assign a developer (e.g. `@hobs`, `@dwayne.negron1`, `@winston.oakley`, `@hmseyoum`, `@billy-horn`) to your MR (they have permission to merge it into the master branch on our repo)

Have fun reviewing your skills and thinking about what you want to learn next!
