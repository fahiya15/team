elastic load balancer
traffic
fargate
ecs
public subnets
cloud formation
cloud watch
application load balancer (layer 3 or 4 traffic, TCP)
network load balancer (don't work well for fargate and EKS) but they are faster for those ISO layers 

ecs.yml - example
vpc.yml


https://docs.aws.amazon.com/elasticloadbalancing/latest/application/introduction.html

Jeremy and Keegan working on a the knowledge graph
https://web.stanford.edu/class/cs520/2020/notes/What_is_a_Knowledge_Graph.html


## QUANTUSflow Software GmbH Stack

- GitHub
    - Markdown (docs)
    - GitHub pages from markdown (GettingStarted and HowTo's)
    - Collaborative code review
    - Version control
    - Issue/feature tracking
    - SourceTree (git GUI)
- Visual Studio Code (IDE)
- CircleCI (CI-CD)
- Prettier / TSLint / ESLint (linting)
- SonarQube (styleguide, code health, linting)
- Docker/Docker-Compose (container management)
- VirtualBox (platform simulation)
- Kubernetes (cluster management)
- Heroku (test environments)
- nginx (facade web server for prod)
- SSLMate+OpenSSL (SSL certs)
- Amazon EC2 + S3 (staging servers)
- PostgreSQL (DB)
- Redis (in-memory DB & caching)

## Viva Translate Stack

- GitHub
    - Markdown (docs)
    - GitHub pages from markdown (GettingStarted and HowTo's)
    - Collaborative code review
    - Version control
    - SourceTree? (git GUI)
- Jira
    - Issue/feature tracking
- Visual Studio Code (IDE)
- GitHub Actions -> CircleCI (CI-CD)
- ESLint (js linting)
- None -> flake8 (py linting)
- None (styleguide, code health)
- Testing
    - None -> Jest (javascript)
    - None -> doctests+pytest(python)
- Docker/Docker-Compose (container management)
- EC2+EBS (platform simulation)
- Lambda -> Terraform (cluster management)
- Local laptops (test environments)
- None -> nginx (facade web server for prod)
- None -> LetsEncrypt certbot (SSL certs)
- Amazon EC2 + S3 (staging servers)
- PostgreSQL (DB)
- Redis (in-memory DB & caching)

## Recommendation reasoning

- GitHub Actions -> CircleCI
    - more compatible with AWS
    - standard API
    - stable UX
- Lambda -> Terraform
    - performance
    - cost
    - scalabilty
    - stable API
    - customization
- None -> flake8 (py linting)
- Testing 
    - None -> Jest (javascript)
        - [tutorial](https://medium.com/2359media/5-techniques-for-frontend-testing-with-jest-and-enzyme-8e926b3c92f8)
        - most popular and oldest testing framework, plays nice with React
    - None -> doctests+pytest(python)
