# Updating a fork based on origin/master

Fetch and checkout the source remote (`Ikwuakor`) branch (`master`) that the merge request is for. You can find these lines within the link for "You can merge these changes from the [command line]"

```console
$ git fetch "git@gitlab.com:Ikwuakor/qary.git" master
$ git checkout -b "Ikwuakor/qary-master" FETCH_HEAD
```

You want to fetch all the latest code from the official master branch.
The official repository that you forked us usually called `upstream`. 
So if you cloned your fork all you need to do is add the upstream remote for the source of the Fork that you're trying to merge into. 
`TangibleAI` is the name of the organization that you forked qary from, so you probably want to do it like this:

```console
git remote add upstream git@gitlab.com:tangibleai/qary
```

You want to fetch all the latest code from the official master branch.
Then merge the updated official master into Uzi's `master`.

```console
$ git fetch upstream master
$ git merge master
```

Then run the unittests to make sure the tests pass before merging into master.

```console
$ pytest
```

Because this fork used a new Python package called `pronouncing` that I hadn't installed on my environment, `pytest` failed for me:

```console
========================================================================== test session starts ===========================================================================
platform linux -- Python 3.8.5, pytest-6.2.2, py-1.10.0, pluggy-0.13.1 -- /home/hobs/anaconda3/envs/qaryenv/bin/python
cachedir: .pytest_cache
rootdir: /home/hobs/code/tangibleai/qary, configfile: pytest.ini, testpaths: tests, src/qary
plugins: cov-2.11.1
collected 152 items / 1 error / 151 selected                                                
...
======================================================================== short test summary info =========================================================================
ERROR src/qary/skills/rap.py - ModuleNotFoundError: No module named 'pronouncing'
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Interrupted: 1 error during collection !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
```

Fortunately Uzi correctly updated `requirements.txt` and `setup.cfg` with the package named pinned to the correct version number.
So all I needed to do was `pip install -r scripts/requirements.txt` before rerunning `pytest`:

```
$ pip install -r scripts/requirements.txt
$ pytest
```

This time all the tests passed!

```
============== test session starts ============
platform linux -- Python 3.8.5, pytest-6.2.2, py-1.10.0, pluggy-0.13.1 -- /home/hobs/anaconda3/envs/qaryenv/bin/python
cachedir: .pytest_cache
rootdir: /home/hobs/code/tangibleai/qary, configfile: pytest.ini, testpaths: tests, src/qary
plugins: cov-2.11.1
collected 152 items 
...
-----------------------------------------------------------------------
TOTAL                                  4633   1507   1717    202    64%

============= 152 passed, 8 warnings in 288.83s (0:04:48) =============
```

Coverage did go down a bit with Uzi's new feature. Coverage fell from 65% to 64%.
This is because Uzi didn't write any unittests in the `tests/` directory and didn't write and doctests in his `rap.py` file either.
If you want to be a superstar on your software development team, you can make sure your code always comes with unittests or doctests. 
That way your team doesn't accumulate *technical debt*.
Plus they understand a bit better how your code is supposed to be used. 
They might find other uses for some of your functions or classes.

Now that tests are passing on the local version of Uzi's fork, all that's left to do is to merge the new code into master:

```console
$ git fetch origin -a
$ git checkout "master"
$ git merge --no-ff "Ikwuakor/qary-master"
```

You can run pytest one last time, if you like, before pushing up to master on the official repostiory.

```
$ pytest

----------------------------------------------------------------------
TOTAL                                 4633   1507   1717    202    64%

============= 152 passed, 8 warnings in 288.83s (0:04:48) ============
```

So you can confidently push your shiny new feature up to the official master branch and incorporate it into any production apps, without breaking anything.

