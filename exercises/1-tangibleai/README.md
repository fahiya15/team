# TangibleAI

## Table of Contents
1. [Who are We?](#who-are-we)
    a. [Mission Statement](#mission-statement)
    b. [Guiding Principles](#guiding-principles)
    c. [History](#history)
    d. [Staff](#staff)
2. [Company Structure](#company-structure)
3. [Current Projects](#current-projects)
4. [Past Projects](advanced-techniques)


### Who are We? 
[[TangibleAI Introduction Video](https://imgur.com/a/wfva9xB.png)]](https://tan.sfo2.digitaloceanspaces.com/videos/2020-10-30--intro_to_tangible_ai.mp4")

#### Mission Statement

** Make AI accessible and affordable to those solving humanity's most pressing problems. ** 

#### Guiding Principles
#### History
#### Staff

### Company Structure

### Current Projects

### Past Projects
