Slides are what i use to plan and script a lecture. I like to use other tools for homework assignments, exercises, quizzes, or assessments. What format does Springboard use for quizzes or interactive lessons for students?

### datasets

https://simple.m.wikipedia.org/wiki/List_of_countries_by_area

https://en.m.wikipedia.org/wiki/Lists_of_countries_and_territories

https://en.m.wikipedia.org/wiki/World_Happiness_Report

### import as vs =
import pandas
pd = pandas

this??
this.__file__
__name__


### method vs function

list of dictionaries
nested dictionaries

& vs and

str vs repr

class vs def

callable __call__ Class()

np.nan > 0

x vs X hobbgoblin of little minds
shape == (16, 16, 1) grayscale, color

vars dir

.__dict__[attr] vs .attr

list comprehension with conditional filter and conditional values

lambda functions and apply

generator

## Pandas objects &
df = pd.read_csv(fn, index_col=0)
df.to_csv(fn, index=False)



adzuna
shopper@totalgood.com
hobs
gratefulforpublicapis

Application: ttatantangttangible ai's App
Default application created on signup.
Application ID

This is the application ID, you should send with each API request.
7fc41b79
Application Keys
These are application keys used to authenticate requests.
8ac1c22c3a214cc6a2b09903316ed917    —
Properties
State   live
Edit
Plan: Trial Access (Review/Change)
To extend your limits please contact us.













### Resources

https://realpython.com/quizzes/

https://bogotobogo.com/python/python_interview_questions.php

https://www.educative.io/courses/learn-python-3-from-scratch

https://opensource.com/article/20/9/teach-python-jupyter

https://gist.github.com/lheagy/f216db7220713329eb3fc1c2cd3c7826

Have you or the springboard learning engineers read the book on using jupyter notebooks for teaching?
Here's a really important chapter. https://jupyter4edu.github.io/jupyter-edu-book/catalogue.html

Here are some random thoughts on topics to cover in lecture 2


## Free Form Quiz Questions

Name at least 5 ways to get help without using the internet.

```python
help()
?
??
print()
```

[tab]-completion ( autocomplete )
ctrl-R  ( search for previously typed commands )
up arrow ( scroll through previous commands )

Which line(s) of a Traceback are the most important to read and understand when you get an error message?

1. the last two lines
2. the first two lines

What important information should you look for in an error message (Traceback)?

1. the line number of your code that failed
2. the error type

What is the most insidious type of programming error?

When your code invisibly (quietly) does something to a variable or data that you did not intend.

How can you find and correct it?

Examine your variables after every line of code to ensure what you intended to happen actually worked. And make sure there were no unintended "side effects".  Every Jupyter notebook cell that you create should have a single line of code followed by a bare variable name, so that you can examine the variable you intended to modify and confirm.

Why is it so important to run jupyter notebooks top to bottom every single time you run them?

because code is procedural, each cell is modifying the global names space. so one cell can undo or redo the work of another cell if you're not careful, creating duplicates or corrupted data.

What is a good way to quickly try some lines of code out and explore a new package or module?

one line at a time in ipython (anaconda console) NOT Jupyter notebook

how can you export the history of all your ipython code to the terminal or a file?

history 
history -o -p
history -f filename.py

How can you modularize your python code to make it more reliable and reusable?

create functions

How can you automate repeated operations on data?

for loop

What is DRY code?

do not repeat yourself

How do you create a python module?

Text editor to create a py file.

What is a good IDE or text editor for Python files?

vscode, atom, or sublime text (NOT Jupyter notebook and NOT windows Notepad)

How do you import your own python modules into a jupyter notebook?



## built in functions

Name at least 5 useful builtin functions.

```python
str()
repr()
len()
print()
sorted()
reversed()
```

## built-in numerical types

Name two python data types for storing a single number.

int
float


## everything is an object

```
dict.get()
duct.update()
__str__
__repr__
```

## built-in types

int
float

## builtin container for text

str

What does this output:
```python
text = "Hello"
text
```

```python
text[1]
```

```
"Hello"[1]
```

## built-in container types

```
str
list
set
dict
```


nums = [1, 2, 4]
nums

nums = nums.append(8)
nums

nums = nums.extend([16, 32])
x 

 = list(''abc")

## external containers for numerical data

Name 3 containers for numerical data. Hint, where do we usually store data for data science?

```
pandas.DataFrame
pandas.Series
numpy.array
```

What does this output:


```
import numpy
x = numpy.array([1,2,4,8])
x
```

```
import pandas
s = pandas.Series(range(4))
s + pandas.Series(range(0, -4, -1))
```


## modules

Name 4 python packages or modules (imports) that you use in data science.


math
Pandas
Sci
create your own module

### datasets

https://simple.m.wikipedia.org/wiki/List_of_countries_by_area

https://en.m.wikipedia.org/wiki/Lists_of_countries_and_territories

https://en.m.wikipedia.org/wiki/World_Happiness_Report

### import as vs =
import pandas
pd = pandas

this??
this.__file__
__name__


### method vs function

list of dictionaries
nested dictionaries

& vs and

str vs repr

class vs def

callable __call__ Class()

np.nan > 0

x vs X hobbgoblin of little minds
shape == (16, 16, 1) grayscale, color

vars dir

.__dict__[attr] vs .attr

list comprehension with conditional filter and conditional values

lambda functions and apply

generator

## Pandas objects &
df = pd.read_csv(fn, index_col=0)
df.to_csv(fn, index=False)



adzuna
shopper@totalgood.com
hobs
gratefulforpublicapis

Application: ttatantangttangible ai's App
Default application created on signup.
Application ID

This is the application ID, you should send with each API request.
7fc41b79
Application Keys
These are application keys used to authenticate requests.
8ac1c22c3a214cc6a2b09903316ed917    —
Properties
State   live
Edit
Plan: Trial Access (Review/Change)
To extend your limits please contact us.













### Resources

https://realpython.com/quizzes/

https://bogotobogo.com/python/python_interview_questions.php

https://www.educative.io/courses/learn-python-3-from-scratch

https://opensource.com/article/20/9/teach-python-jupyter

https://gist.github.com/lheagy/f216db7220713329eb3fc1c2cd3c7826

Have you or the springboard learning engineers read the book on using jupyter notebooks for teaching?
Here's a really important chapter. https://jupyter4edu.github.io/jupyter-edu-book/catalogue.html

Here are some random thoughts on topics to cover in lecture 2


## Free Form Quiz Questions

Name at least 5 ways to get help without using the internet.

```python
help()
?
??
print()
```

[tab]-completion ( autocomplete )
ctrl-R  ( search for previously typed commands )
up arrow ( scroll through previous commands )

Which line(s) of a Traceback are the most important to read and understand when you get an error message?

1. the last two lines
2. the first two lines

What important information should you look for in an error message (Traceback)?

1. the line number of your code that failed
2. the error type

What is the most insidious type of programming error?

When your code invisibly (quietly) does something to a variable or data that you did not intend.

How can you find and correct it?

Examine your variables after every line of code to ensure what you intended to happen actually worked. And make sure there were no unintended "side effects".  Every Jupyter notebook cell that you create should have a single line of code followed by a bare variable name, so that you can examine the variable you intended to modify and confirm.

Why is it so important to run jupyter notebooks top to bottom every single time you run them?

because code is procedural, each cell is modifying the global names space. so one cell can undo or redo the work of another cell if you're not careful, creating duplicates or corrupted data.

What is a good way to quickly try some lines of code out and explore a new package or module?

one line at a time in ipython (anaconda console) NOT Jupyter notebook

how can you export the history of all your ipython code to the terminal or a file?

history 
history -o -p
history -f filename.py

How can you modularize your python code to make it more reliable and reusable?

create functions

How can you automate repeated operations on data?

for loop

What is DRY code?

do not repeat yourself

How do you create a python module?

Text editor to create a py file.

What is a good IDE or text editor for Python files?

vscode, atom, or sublime text (NOT Jupyter notebook and NOT windows Notepad)

How do you import your own python modules into a jupyter notebook?



## built in functions

Name at least 5 useful builtin functions.

```python
str()
repr()
len()
print()
sorted()
reversed()
```

## built-in numerical types

Name two python data types for storing a single number.

int
float


## everything is an object

```
dict.get()
duct.update()
__str__
__repr__
```

## built-in types

int
float

## builtin container for text

str

What does this output:
```python
text = "Hello"
text
```

```python
text[1]
```

```
"Hello"[1]
```

## built-in container types

```
str
list
set
dict
```


nums = [1, 2, 4]
nums

nums = nums.append(8)
nums

nums = nums.extend([16, 32])
x 

 = list(''abc")

## external containers for numerical data

Name 3 containers for numerical data. Hint, where do we usually store data for data science?

```
pandas.DataFrame
pandas.Series
numpy.array
```

What does this output:


```
import numpy
x = numpy.array([1,2,4,8])
x
```

```
import pandas
s = pandas.Series(range(4))
s + pandas.Series(range(0, -4, -1))
```


## modules

Name 4 python packages or modules (imports) that you use in data science.


math
Pandas
Sci
create your own module
