# create some spacy Matcher objects to generate these entailment statements
- 
  source: https://journals.physiology.org/doi/full/10.1152/ajpheart.01162.2007
  abstract: |+
    The cardiovascular benefits of light to moderate red wine consumption often have been attributed to its polyphenol constituents. However, the acute dose-related hemodynamic, vasodilator, and sympathetic neural effects of ethanol and red wine have not been characterized and compared in the same individual. We sought to test the hypotheses that responses to one and two alcoholic drinks differ and that red wine with high polyphenol content elicits a greater effect than ethanol alone. Thirteen volunteers (24–47 yr; 7 men, 6 women) drank wine, ethanol, and water in a randomized, single-blind trial on three occasions 2 wk apart. One drink of wine and ethanol increased blood alcohol to 38 ± 2 and 39 ± 2 mg/dl, respectively, and two drinks to 72 ± 4 and 83 ± 3 mg/dl, respectively. Wine quadrupled plasma resveratrol (P < 0.001) and increased catechin (P < 0.03). No intervention affected blood pressure. One drink had no heart rate effect, but two drinks of wine increased heart rate by 5.7 ± 1.6 beats/min; P < 0.001). Cardiac output fell 0.8 ± 0.3 l/min after one drink of ethanol and wine (both P < 0.02) but increased after two drinks of ethanol (+0.8 ± 0.3 l/min) and wine (+1.2 ± 0.3 l/min) (P < 0.01). One alcoholic drink did not alter muscle sympathetic nerve activity (MSNA), while two drinks increased MSNA by 9–10 bursts/min (P < 0.001). Brachial artery diameter increased after both one and two alcoholic drinks (P < 0.001). No beverage augmented, and the second wine dose attenuated (P = 0.02), flow-mediated vasodilation. One drink of ethanol dilates the brachial artery without activating sympathetic outflow, whereas two drinks increase MSNA, heart rate, and cardiac output. These acute effects, which exhibit a narrow dose response, are not modified by red wine polyphenols.

    The concept of a protective effect of light to moderate alcohol intake against coronary heart disease, ischemic stroke, and heart failure has been distilled from the results of >60 observational studies (13, 16, 22). For example, in the Male Health Professional study (n = 38,077), compared with less than one drink per week, alcohol consumption between 3 and 7 days per week was associated with approximately one-third less risk of myocardial infarction (22). A meta-analysis involving 10 studies (n = 176,042) provides further support for the existence of a J-shaped rather than linear relationship between the use of alcohol and the risk of suffering a cardiovascular event (7, 16). Although it has been hypothesized that polyphenols in red wine should contribute to such benefit, no clear distinction between alcoholic beverages with respect to cardiovascular risk reduction has emerged from these analyses (16, 22). Consequently, the American Heart Association Nutrition Committee does not comment on whether any particular alcoholic beverage is preferred but advises that men who drink alcohol should limit their consumption to two drinks per day and women to one daily drink. In this context, because they contain a comparable amount of alcohol (15–18 g), each of a 12-oz bottle of beer (355 ml), a 4-oz glass of wine (120 ml), and a 1.5-oz (44 ml) shot of spirits is considered one standard “drink” (10, 18).

    The potential benefits of alcohol may relate to its metabolic, antithrombotic, anticoagulant, antioxidant, or anti-inflammatory properties or to effects on hemodynamics, vascular endothelial function, and neurohumoral regulation of the circulation; these latter actions are the focus of the present study. Intoxicating doses of ethanol have been shown to dilate conduit arteries, increase heart rate (HR), and stimulate sympathetic nervous system activity, causing an increase in blood pressure that is sustained for hours after vasodilation dissipates (3, 11, 26, 30). These acute sympathoexcitatory and pressor effects, if replicated with each drink, could account in part for the sustained elevations in ambulatory awake blood pressure and asleep HR observed in a randomized crossover trial when 40 g of ethanol was consumed daily for 4 wk as either beer or red wine (35) and could also contribute to the dose-dependent relationship with blood pressure observed when more than two alcoholic drinks daily are consumed chronically (24). In contrast, there has been no systematic dose-response evaluation of the hemodynamic and vascular effects of one and two standard drinks of red wine or ethanol in the same person, or any direct comparison of such responses between red wine and ethanol.

    The purpose of this randomized, single-blind, water-controlled study involving healthy subjects was to answer four questions: 1) Does low or moderate red wine and/or alcohol ingestion affect central sympathetic outflow and endothelium-dependent dilation? 2) If so, are these effects dose dependent? 3) What is the relationship between the amount of wine or ethanol ingested and blood pressure or sympathetic nerve traffic? 4) Does a red wine with verified high polyphenol content differ from alcohol in its effects on vascular responsiveness or the sympathetic nervous system?

  sentences:
    -
      sentence: "Thirteen volunteers (24–47 yr; 7 men, 6 women) drank wine, ethanol, and water in a randomized, single-blind trial on three occasions 2 wk apart."
      variables:
        subject_min_age: {24: yr}
        subject_max_age: {47: yr}
        treatments: [wine, ethanol]
        placebo: [water]
        num_subjects: 13
        num_men: 7
        num_women: 6
      substitutions:
        men: [males, adult males]
        women: [females, adult females]
        trial: [study]
        randomized, single-blind: [single-blind, single-blind clinical]
        volunteers: [subjects, participants, volunteer subjects, volunteer participants]
        drank: [consumed, imbibed, were directed to drink, voluntarily drank]
        ethanol: [alcohol, grain alcohol, food grade alcohol]
        on three occasions: [in three sittings, at three appointments, on three visits]
        three: [3]
        2 wk: [two weeks]
        2 wk apart: [at week 1, week 3, and week 5 of the study]
        24–47 yr: [24 to 47 years old, age 24 to 47 yr, age 24 to 47]
      qa_training_set: 
        Were there an equal number of women and men subjects in the study?:
          - No
          - Approximately, but there was one more male subject.
          - No. There were 7 male and 6 female adult subjects in the study.
          - "No" 
        What was the age range of the subjects in the study?:
          - 24 to 47 years old
          - The minimum age of the subjects was 24. The maximum age was 47.
          - The subjects were between 24 and 47 years old, inclusive.
      quiz_questions:
        What makes a trial "double-blind"?
          - Neither the participants in the clinical trial do not know if they are receiving the placebo or the real treatment. 
        - What makes a clinical trial "single-blind"?
        - Was the nature of the drinks (water vs wine vs ethanol) hidden from the subjects?
        - Was the nature of the drinks hidden from the administrators of the test?
        - Was the nature of the drinks hidden from the data scientist?
        - What is the difference between a single-blind, a double-blind, and a nonblinded clinical trial or study.
        - What about this study made it *single-blind* rather than *double-blind*?
    -
      sentence: "One drink of wine and ethanol increased blood alcohol to 38 +/- 2 and 39 +/- 2 mg/dl, respectively two drinks to 72 +/- 4 and 83 +/- 3 mg/dl, respectively."
      variables:
        mean_blood_alcohol_increase_1_drink_wine: {38: mg/dl} 
        std_blood_alcohol_increase_1_drink_wine: {2: mg/dl} 
        std_blood_alcohol_increase_1_drink_ethanol: {2: mg/dl} 
        mean_blood_alcohol_increase_1_drink_ethanol: {39: mg/dl} 
      logically entails:
        - "One drink of wine increased blood alcohol to 38 +/- 2 mg/dl."
        - "One drink of wine increased blood alcohol to 38 mg/dl on average."
        - "One drink of wine increased blood alcohol from 0 mg/dl to 38 mg/dl on average."
        - "One drink of wine increased blood alcohol from 0 mg/dl to 38 mg/dl +/- 2 mg/dl."
        - "One drink of wine increased blood alcohol to 38 mg/dl with a standard deviation of 2 mg/dl."
        - "One drink of wine increased blood alcohol to a value of 38 +/- 2 mg/dl."
        - "One drink of wine increased blood alcohol to a between 36 and 40 mg/dl (68% confidence interval)."
        - "One drink of wine increased blood alcohol to a between 36 and 40 mg/dl (68% confidence)."
        - "One drink of wine increased blood alcohol to a between 36 and 40 mg/dl 68% of the time."
        - "One drink of wine increased blood alcohol to a between 36 mg/dl and 40 mg/dl 68% of the time."
        - "One drink of ethanol increased blood alcohol to 39 +/- 2 mg/dl."
        - "Two drinks of wine increased blood alcohol to 72 +/- 2 mg/dl."
        - "Two drinks of ethanol increased blood alcohol to 83 +/- 2 mg/dl."
      textually entails:
        - "One glass of wine increases blood alcohol 38 mg/dl."
        - "A drink of wine increases your blood alcohol level to 38 mg/dl."
        - "A single glass of wine increases your blood alcohol level to 38 mg/dl."
        - "One drink of wine increased blood alcohol from 0 mg/dl to 38 mg/dl +/- 2 mg/dl."
        - "One drink of wine increased blood alcohol to 38 mg/dl with a standard deviation of 2 mg/dl."
        - "One drink of wine increased blood alcohol to a value of 38 +/- 2 mg/dl."
        - "One drink of wine increased blood alcohol to a between 36 and 40 mg/dl (68% confidence interval)."
        - "One drink of wine increased blood alcohol to a between 36 and 40 mg/dl (68% confidence)."
        - "One drink of wine increased blood alcohol to a between 36 and 40 mg/dl 68% of the time."
        - "One drink of wine increased blood alcohol to a between 36 mg/dl and 40 mg/dl 68% of the time."
        - "One drink of ethanol increased blood alcohol to 39 +/- 2 mg/dl."
    -
      sentence: "One drink had no heart rate effect, but two drinks of wine increased heart rate by 5.7 ± 1.6 beats/min; P < 0.001)."
      variables:
        mean_heart_rate_change_1_drink_wine: {5.7: bpm}
        std_heart_rate_change_1_drink_wine: {2.6: }
