# Learning How to Think

- be intentional/mindful about your work (don't be a [zombie]())
- ask yourself questions instead of googling
- know how to find help, ?, ??, tab-completion
- learn how to evaulate the quality and appropriateness of an SO answer or blog post by scanning
- identifying important things to do
- breaking down projects into small pieces
- habits to think about what you're doing in the morning
- habits to rethink about what you're doing every hour or less
- prioritizing tasks
- talk to someone familiar with your tech but not your project
- pitch your project to someone unfamiliar with the tech
- rubber duck with yourself to solve problems
- get up and stretch or fix a cup of tea when not in the zone

## Exercise

- add to the skills-checklist.md
- create a todo list for this week
- prioritize tasks for today
- write user stories
- break down a problem into pieces
- solve a logic puzzle by breaking it down
- identify the functions that need to be written to solve a CS problem
- add meetings to your calendar at times in the day when you are unlikely to be in the zone (after lunch)
- listen carefully
- read carefully (error messages included)

