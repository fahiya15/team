# Retrospective

Congratulations! You've completed the internship!

For an Agile company like Tangible AI, whenever you complete something, you want to hold a retrospective, or "retro" meeting.
It's an opportunity to look back and talk about your accomplishments and figure out how to do things better.

So this week, you can do a retrospective on the past 10 weeks.
You can review the skills checklist to see if there are any that you can now check off that list. 
Then you can look at some of the unchecked boxes to think about what you'd like to do with the next 10 weeks.

[]

