# intern-challenge-11-graph-database-schema-in-django

Graphs or networks are a database structure for contain

## Django Models.py Basics

While you're going through the "your first django app" tutorial. See if you can come up with a project you'd like to build in django that uses a graph data structure.


- mind map: nodes for concepts and edges for their relationships extracted from natural language documents
- wikipedia knowledge graph: nodes for wikipedia page titles, edges for hyperlinks between them
- page rank graph: nodes for web pages, edges for hyperlinks between them, rank pages for their importance
- qa graph: between stack-overflow questions and between questions and answers
- social network: links between people in a social network
- knowledge graph: nodes for entities (nouns) and edges for relationships (is_a, contains, related_to, similar_to)
- game graph: a dynamically generated graph based on the possible moves available to you in a game of tic-tac-to, checkers, 8-queens, chess, Go
- conversation graph

## Relational Database Schema for Graphs

And think about how you'd implement it in a Django models.py relational database schema.

#### Hint:

- You'll need a ManyToMany relationship between a `Node` table and itself.
- And you'll need a "through" table to hold all the edges and their properties.

## Graph queries

Implement graph search algoriths within your graph db:

- breadth first search
- depth first search
- A* search
